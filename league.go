package learnGo

import (
	"encoding/json"
	"fmt"
	"io"
)

// League type
type League []Player

// Find return a ptr to a Player
func (l League) Find(name string) *Player {
	for i, p := range l {
		if p.Name == name {
			return &l[i]
		}
	}
	return nil
}

// NewLeague method parse io.Reader and return slice of Player
func NewLeague(rdr io.Reader) (League, error) {
	var league League
	err := json.NewDecoder(rdr).Decode(&league)

	if err != nil {
		err = fmt.Errorf("problem parsing league, %v", err)
	}

	return league, err
}
